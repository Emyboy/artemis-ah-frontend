import React from 'react';

export default () => (
  <h1 className="msg">The requested page was not found.</h1>
);
